package practise.com.demo.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import practise.com.demo.model.LibraryDataDTO;
import practise.com.demo.service.LibraryService;
import practise.com.demo.service.UserService;

@RestController
public class LibraryController {

	LibraryService service;
	UserService users;

	@Autowired
	LibraryController(LibraryService service, UserService users){
		this.service = service;
		this.users = users;
	}

	@PostMapping("/libraries")
	public void setData(@RequestBody @Valid LibraryDataDTO data){
		service.addObserver(users);
		service.setData(data);
	}
}

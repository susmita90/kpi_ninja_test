package practise.com.demo.service;

import java.util.Observable;
import java.util.Observer;

import org.springframework.stereotype.Component;

import practise.com.demo.model.LibraryDataDTO;

@Component
public class UserService implements Observer {

	private LibraryDataDTO data;

	public LibraryDataDTO getData() {
		return data;
	}

	public void setData(LibraryDataDTO data) {
		this.data = data;
	}

	public void update(Observable o, Object arg) {
		
		this.setData((LibraryDataDTO) arg);
		System.out.println("New Document added to Library");
		System.out.println("Document title::" + getData().getTitle() + ",  Document Author::" + getData().getAuthor());
		System.out.println("Mail sent to user...");
	}
}

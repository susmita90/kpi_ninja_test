package practise.com.demo.service;

import java.util.Observable;

import org.springframework.stereotype.Service;

import practise.com.demo.model.LibraryDataDTO;

@Service
public class LibraryService extends Observable {

//	private LibraryDataDTO data;

	public void setData(LibraryDataDTO data) {
		
		// code to persist data in DB
		
//		this.data = data;
		setChanged();
		notifyObservers(data);
	}
}
package practise.com.demo.model;

import javax.validation.constraints.NotBlank;

public class LibraryDataDTO {

	@NotBlank(message = "Author should not be blank")
	String author;

	@NotBlank(message = "Title should not be blank")
	String title;

	public String getAuthor() {
		return author;
	}

	public String getTitle() {
		return title;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		return "LibraryData{" +
				"author='" + author + '\'' +
				", title='" + title + '\'' +
				'}';
	}
}
